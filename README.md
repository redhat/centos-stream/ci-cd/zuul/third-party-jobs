# third-party-jobs

This projects contains the Third Party CI jobs:

- [jobs](./zuul.d/jobs.yaml): define jobs and nodeset
- [roles](./roles/): define jobs Ansible tasks
